import {configureStore} from "@reduxjs/toolkit";
import devicesReducer from "./reducers/devicesReducer";
import locationsReducer from "./reducers/locationsReducer";
const store = configureStore({
    reducer: {
        devices: devicesReducer,
        locations: locationsReducer,
    },
})

export default store;