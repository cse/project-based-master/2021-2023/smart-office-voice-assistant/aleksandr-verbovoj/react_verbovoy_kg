import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "../../axios";

export const fetchDevices = createAsyncThunk(
    'devices/fetchDevices',
    async function () {
        const response = await axios.get('/devices')
        return await response.data.devices
    }
)

export const sendCommand = createAsyncThunk(
    'devices/sendCommand',
    async function (data) {
        const {device_name, command} = data
        const response = await axios.put(`/devices/${device_name}`, {'value': command})
        return await response.data
    }
)

export const devicesSlice = createSlice({
    name: 'devices',
    initialState: {
        devices: [],
        status: null,
        error: null
    },
    reducers: {},
    extraReducers: {
        [fetchDevices.pending]: (state) => {
            state.status = 'loading'
            state.error = null
        },
        [fetchDevices.fulfilled]: (state, action) => {
            state.status = 'fulfilled'
            state.devices = action.payload
        },
        [fetchDevices.rejected]: (state, action) => {
            state.status = 'rejected'
            state.error = action.payload
        },
        [sendCommand.fulfilled]: (state, action) => {
            const {name} = action.payload
            const index = state.devices.findIndex(device => device.name === name)
            state.devices[index] = action.payload
        }
    }
})

export default devicesSlice.reducer