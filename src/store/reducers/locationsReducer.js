import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "../../axios";

export const fetchLocations = createAsyncThunk(
    'locations/fetchLocations',
    async function (data) {
        const {start_node} = data
        const response = await axios.get(`/locations?start_node=${start_node}`)
        return await response.data.locations
    }
)

export const fetchLocationInfo = createAsyncThunk(
    'locations/fetchLocationInfo',
    async function (data) {
        const {location_name} = data
        const response = await axios.get(`/locations/${location_name}`)
        return await response.data
    }
)

export const locationSlice = createSlice({
    name: 'locations',
    initialState: {
        locations: [],
        status: null,
        error: null
    },
    reducers: {},
    extraReducers: {
        [fetchLocations.pending]: (state) => {
            state.status = 'loading'
            state.error = null
        },
        [fetchLocations.fulfilled]: (state, action) => {
            state.status = 'fulfilled'
            state.locations = action.payload
        },
        [fetchLocations.rejected]: (state, action) => {
            state.status = 'rejected'
            state.error = action.payload
        },
        [fetchLocationInfo.fulfilled]: (state, action) => {
            const name = action.payload.name
            const index = state.locations.findIndex(location => location.name === name)
            state.locations[index].devices_name = action.payload.devices_name
        }
    }
})

export default locationSlice.reducer