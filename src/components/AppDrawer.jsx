import React from 'react';
import {Box, Drawer, List, ListItem, ListItemButton, ListItemText} from "@mui/material";
import {NavLink} from "react-router-dom";

const AppDrawer = ({children}) => {
    const drawerWidth = 160;

    const menuItem = [
        // {
        //     name: 'PageHome',
        //     path: '/',
        // },
        {
            name: 'Locations',
            path: '/locations',
        },
        {
            name: 'Devices',
            path: '/devices',
        },

        // {
        //     name: 'PageFloorplans',
        //     path: '/floorplans',
        // },
    ]

    return (
        <Box sx={{display: 'flex'}}>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="permanent"
                anchor="left"
            >
                <List>
                    {menuItem.map((item, index) => (
                        <ListItem key={item.name} disablePadding>
                            <ListItemButton component={NavLink} to={item.path} key={index} className={'link'}>
                                <ListItemText className={'link_text'} primary={item.name}/>
                            </ListItemButton>
                        </ListItem>
                    ))}
                </List>
            </Drawer>
            <Box
                component="main"
                sx={{flexGrow: 1, width: 'auto', p: 2}}
            >
                {children}
            </Box>
        </Box>
    );
};

export default AppDrawer;