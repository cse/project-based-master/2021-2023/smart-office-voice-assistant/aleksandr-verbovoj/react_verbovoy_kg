import React from 'react';
import {Box, Typography} from "@mui/material";
import {connect} from "react-redux";
import DeviceSwitchButton from "../../ui/device/DeviceSwitchButton";
import DeviceRollerShutterButtonGroup from "../../ui/device/DeviceRollerShutterButtonGroup";

const DeviceInLocation = (props) => {
    const {device} = props

    const getButtonGroupByDeviceType = (device_type) => {
        switch (device_type) {
            case 'Switch':
                return <DeviceSwitchButton device={device}/>
            case 'Rollershutter':
                return <DeviceRollerShutterButtonGroup device={device}/>
            default:
                return <Box>{device.type_}: {device.state.value}</Box>
        }
    }
    return (
        <Box component={'div'} sx={{display: 'flex'}}>
            <Box sx={{flexGrow: 1}}>
                <Typography>{device.name}</Typography>
            </Box>
            <Box>
                {getButtonGroupByDeviceType(device.type_)}
            </Box>
        </Box>
    );
};

const mapStateToProps = (state, oldProp) => {
    const {device_name} = oldProp
    const device = state.devices.devices.find((device) => device.name === device_name)
    return {"device": device}
}

export default connect(mapStateToProps)(DeviceInLocation);