import React, {useState} from 'react';
import {
    Box,
    Card,
    CardActionArea,
    CardContent,
    Typography
} from "@mui/material";
import LocationDialog from "./LocationDialog";
import {useDispatch} from "react-redux";
import {fetchLocationInfo} from "../../../store/reducers/locationsReducer";

const LocationCard = ({location}) => {
    const [open, setOpen] = useState(false)
    const dispatch = useDispatch()

    const handleOpen = () => {
        setOpen(true)
        dispatch(fetchLocationInfo({'location_name': location.name}))
    }
    const handleClose = () => {
        setOpen(false)
    }

    return (
        <Box>
            <Card variant={'outlined'} sx={{maxHeight: 200, maxWidth: 250}}>
                <CardActionArea onClick={handleOpen}>
                    <CardContent>
                        <Typography variant={'h5'} component={'div'}> {location.label} </Typography>
                        <Typography variant={'h6'} component={'div'}> {location.parent_name} </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
            <LocationDialog
                open={open}
                onCLose={handleClose}
                location={location}
            />

        </Box>
    );
};

export default LocationCard;