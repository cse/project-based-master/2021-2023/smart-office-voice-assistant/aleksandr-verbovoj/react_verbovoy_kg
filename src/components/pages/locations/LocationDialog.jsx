import React from 'react';
import {Dialog, DialogContent, DialogTitle, Divider, Typography} from "@mui/material";
import DeviceInLocation from "./DeviceInLocation";

const LocationDialog = ({open, onCLose, location}) => {
    return (
        <Dialog
            open={open}
            onClose={onCLose}
            fullWidth={true}
            maxWidth={'sm'}
        >
            <DialogTitle variant={'h5'}>
                {location.label}
                <Divider/>
                <Typography>{location.parent_name}</Typography>
            </DialogTitle>
            <DialogContent>
                {location.devices_name != null && location.devices_name.map((device_name) =>
                    <DeviceInLocation device_name={device_name} key={device_name}/>
                )}
            </DialogContent>
        </Dialog>
    );
};

export default LocationDialog;