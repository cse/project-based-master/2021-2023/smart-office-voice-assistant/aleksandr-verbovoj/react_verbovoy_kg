import React from 'react';
import {Box, Stack, Typography} from "@mui/material";

const PageHome = () => {
    return (
        <Box component={'div'}>
            <Stack direction={'row'} spacing={2} component={'div'} className={'page-header'} marginBottom={5}>
                <Typography variant={'h4'}>Home Page</Typography>
            </Stack>
        </Box>
    );
};

export default PageHome;