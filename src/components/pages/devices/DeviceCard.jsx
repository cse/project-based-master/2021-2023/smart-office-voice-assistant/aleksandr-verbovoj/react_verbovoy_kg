import React from 'react';
import {Box, Card, CardActions, CardContent, Typography} from "@mui/material";
import DeviceSwitchButton from "../../ui/device/DeviceSwitchButton";
import DeviceRollerShutterButtonGroup from "../../ui/device/DeviceRollerShutterButtonGroup";
import DeviceDimmer from "../../ui/device/DeviceDimmer";

const DeviceCard = ({device}) => {

    const getButtonGroupByDeviceType = (device_type) => {
        switch (device_type) {
            case 'Switch':
                return <DeviceSwitchButton device={device}/>
            case 'Rollershutter':
                return <DeviceRollerShutterButtonGroup device={device}/>
            case 'Dimmer':
                return <DeviceDimmer device={device}/>
            default:
                return <Box>{device.type_}: {device.state.value}</Box>
        }
    }

    return (
        <Box>
            <Card sx={{maxWidth: 600}} variant={'outlined'}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {device.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        State: {device.state.value} <br/>
                        Type: {device.state.type_}
                    </Typography>
                    <CardActions disableSpacing>
                        {getButtonGroupByDeviceType(device.type_)}
                    </CardActions>
                </CardContent>
            </Card>
        </Box>
    );
};

export default DeviceCard;