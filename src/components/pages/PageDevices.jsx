import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import DeviceCard from "./devices/DeviceCard";
import {Box, Button, Grid} from "@mui/material";
import {fetchDevices} from "../../store/reducers/devicesReducer";

const PageDevices = () => {
    const devices = useSelector(state => state.devices.devices)
    // const {status, error} = useSelector(state => state.devices)
    const dispatch = useDispatch()

    const onUpdateButton = () => {
        dispatch(fetchDevices())
    }

    return (
        <Box component={'div'}>
            <Button varian='contained' size='large'
                    onClick={onUpdateButton}>UPDATE
            </Button>
            <Grid container gridTemplateColumns={'repeat(2, 1fr)'} gap={'17px'}>
                {devices.map((device) =>
                    <DeviceCard device={device} key={device.name}/>
                )}
            </Grid>
        </Box>
    );
};

export default PageDevices;