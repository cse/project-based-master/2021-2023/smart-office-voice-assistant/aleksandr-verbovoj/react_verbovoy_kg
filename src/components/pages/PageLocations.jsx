import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchLocations} from "../../store/reducers/locationsReducer";
import LocationCard from "./locations/LocationCard";
import {Box, Button, Grid, Typography} from "@mui/material";
import {fetchDevices} from "../../store/reducers/devicesReducer";

const PageLocations = () => {

    const locations = useSelector(state => state.locations.locations)
    const dispatch = useDispatch()
    // const {status, error} = useSelector(state => state.locations)

    useEffect(() => {
        dispatch(fetchLocations({'start_node': 'FirstFloor'}))
        dispatch(fetchDevices())
    }, [dispatch])

    const onUpdateButton = () => {
        dispatch(fetchLocations({'start_node': 'FirstFloor'}))
        dispatch(fetchDevices())
    }

    return (
        <Box component={'div'}>
            <Box spacing={2} component={'div'} className={'page-header'}>
                <Typography variant={'h4'}>Офис</Typography>
            </Box>

            <Button varian='contained' size='large'
                    onClick={() => onUpdateButton()}>UPDATE</Button>

            <Grid container gridTemplateColumns={'repeat(4, 1fr)'} gap={'17px'}>
                {locations.map((location) =>
                    <LocationCard location={location} key={location.name}/>
                )}
            </Grid>
        </Box>
    );
};

export default PageLocations;