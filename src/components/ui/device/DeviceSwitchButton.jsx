import React, {useState} from 'react';
import {Switch} from "@mui/material";
import {sendCommand} from "../../../store/reducers/devicesReducer";
import {useDispatch} from "react-redux";

const DeviceSwitchButton = ({device}) => {

    const dispatch = useDispatch()
    const [checked, setChecked] = useState(device.state.value === 'ON')

    const toggleSwitch = () => {
        setChecked(!checked)
    }

    const handleOnChange = () => {
        toggleSwitch()
        const n_value = checked ? 'OFF' : 'ON'
        const changeStateData = {'device_name': device.name, 'command': n_value}
        console.log(changeStateData)
        dispatch(sendCommand(changeStateData))
    }

    return (
        <Switch checked={checked} onChange={handleOnChange}/>
    );
};

export default DeviceSwitchButton;