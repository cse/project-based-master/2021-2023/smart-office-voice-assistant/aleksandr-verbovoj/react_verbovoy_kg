import React from 'react';
import {Button, ButtonGroup} from "@mui/material";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import StopIcon from "@mui/icons-material/Stop";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import {sendCommand} from "../../../store/reducers/devicesReducer";
import {useDispatch} from "react-redux";

const DeviceRollerShutterButtonGroup = ({device}) => {
    const dispatch = useDispatch()

    const handleRS = (n_value) => {
        const changeStateData = {'device_name': device.name, 'command': n_value}
        console.log(changeStateData)
        dispatch(sendCommand(changeStateData))
    }
    return (
        <ButtonGroup size="small" variant="outlined" aria-label="outlined button group">
            <Button aria-label="UP" onClick={() => handleRS('UP')}><ArrowUpwardIcon/></Button>
            <Button aria-label="STOP" onClick={() => handleRS('STOP')}><StopIcon/></Button>
            <Button aria-label="DOWN" onClick={() => handleRS('DOWN')}><ArrowDownwardIcon/></Button>
        </ButtonGroup>
    );
};

export default DeviceRollerShutterButtonGroup;