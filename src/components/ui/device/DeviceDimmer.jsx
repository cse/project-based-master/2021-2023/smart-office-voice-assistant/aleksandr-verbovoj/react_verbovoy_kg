import React, {useState} from 'react';
import {Slider} from "@mui/material";
import {sendCommand} from "../../../store/reducers/devicesReducer";
import {useDispatch} from "react-redux";

const DeviceSlider = ({device}) => {

    const [sliderValue, setSliderValue] = useState(device.state.value)
    const dispatch = useDispatch()

    const handleOnChange = () => {
        const changeStateData = {'device_name': device.name, 'command': sliderValue}
        console.log(changeStateData)
        dispatch(sendCommand(changeStateData))

    }
    return (
        <Slider
            size={"small"}
            defaultValue={sliderValue}
            marks
            min={0}
            max={100}
            step={10}
            onChange={handleOnChange}
        ></Slider>
    );
};

export default DeviceSlider;