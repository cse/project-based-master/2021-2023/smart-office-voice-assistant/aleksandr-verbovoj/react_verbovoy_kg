import React from "react";
import '../src/styles/main.css'
import {BrowserRouter, Route, Routes} from "react-router-dom";
import PageHome from "./components/pages/PageHome";
import PageDevices from "./components/pages/PageDevices";
import PageFloorplans from "./components/pages/PageFloorplans";
import PageLocations from "./components/pages/PageLocations";
import AppDrawer from "./components/AppDrawer";
import {Box, ThemeProvider} from "@mui/material";
import {themeOptions} from "./styles/MUITheme";

function App() {
    return (
        <div className="App">
            <ThemeProvider theme={themeOptions}>
                <Box sx={{display: 'flex'}}>
                    <BrowserRouter>
                        <AppDrawer>
                            <Routes>
                                <Route path={"/"} element={<PageHome/>}/>
                                <Route path={"/devices"} element={<PageDevices/>}/>
                                <Route path={"/locations"} element={<PageLocations/>}/>
                                <Route path={"/floorplans"} element={<PageFloorplans/>}/>
                            </Routes>
                        </AppDrawer>
                    </BrowserRouter>
                </Box>
            </ThemeProvider>
        </div>
    );
}

export default App;
