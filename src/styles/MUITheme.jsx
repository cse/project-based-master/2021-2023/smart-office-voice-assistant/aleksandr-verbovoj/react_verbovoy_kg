import { createTheme } from '@mui/material/styles';

export const themeOptions = createTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#6B9080',
        },
        secondary: {
            main: '#f50057',
        },
        background: {
            default: '#F6FFF8',
            paper: '#EAF4F4',
        },
    },
});